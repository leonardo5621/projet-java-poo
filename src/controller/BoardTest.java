package controller;

import model.ColorSet;
import model.Direction;
import model.Pipe;
import model.Point;
import model.Square;
import model.Terminal;

/**
 * Plateau du jeu où pour performer des tests
 * @author leonardo
 *
 */
public class BoardTest implements IBoard {

	private Square [][] squares;
	private int columns;
	private int rows;
	private Point[] terminals = {new Point(0,0), new Point(0,1), new Point(4,0), new Point(4,1)};
	
	public BoardTest(int rows, int columns) {
		// TODO Auto-generated constructor stub
		this.rows = rows;
		this.columns = columns;
		this.squares = new Square[rows][columns];
		this.startBoard();
	}


	@Override
	public void startBoard() {
		for(int i=0; i<rows; i++) {
			for(int j=0; j<columns; j++) {
				squares[i][j] = new Square(this);
			}
		}
		
		for(Point p: terminals) {
			ColorSet chosenColor = ColorSet.values()[p.getCol()];
			Terminal toAssign = new Terminal(chosenColor, squares[p.getRow()][p.getCol()]);
			squares[p.getRow()][p.getCol()].assignTerminal(toAssign);
		}

		
	}

	@Override
	public void displayBoard() {
		for(int i=0; i<rows; i++) {
			System.out.print(" | ");
			for(int j=0; j<columns; j++) {
				if(squares[i][j].getTerminal() != null) {
					System.out.print(" T ");
				} else {
					Pipe passPipe = squares[i][j].getPipe(); 
					if( passPipe != null) {
						System.out.print(passPipe.getColor().colorChar);
					} else {
						System.out.print(" X ");
					}
				}
			}
			System.out.print(" | ");
			System.out.println("  ");
		}
		System.out.println("  ");

	}

	@Override
	public int getRows() {
		return this.rows;
	}

	@Override
	public int getColumns() {
		return this.columns;
	}

	@Override
	public Square[][] getSquares() {
		return this.squares;
	}

	@Override
	public Square getSquare(int i, int j) {
		return this.squares[i][j];
	}

	@Override
	public Square getNeighbor(Square sq, Direction dir) {
		for(int i=0; i<rows; i++) {
			for(int j=0; j<columns; j++) {
				if(squares[i][j] == sq) {
					return this.getNeighboringSquare(i, j, dir);
				}
			}
		}
		return null;

	}
	
	public Square getNeighboringSquare(int i, int j, Direction dir) {
		
		switch(dir) {
		case RIGHT:
			j = j == columns-1 ? j : j+1;
			break;
		case LEFT:
			j = j == 0 ? 0 : j-1;
			break;
		case UP:
			i = i == 0 ? 0 : i-1;
			break;
		case DOWN:
			i = i == rows ? i : i+1;
			break;
		}
		
		return squares[i][j];
	}
	
	@Override
	public void collapseSameColorPipe(Terminal newTerminal) {
		
	}

}
