package controller;

import model.Direction;
import model.Square;
import model.Terminal;

public interface IBoard {
	
	/**
	 * Méthode appellé par le constructeur qui effectivement
	 * crée les instances pour les cases et attribue les plots
	 */
	void startBoard();
	
	/**
	 * Méthode d'affichage du plateau en ligne de commande
	 * @deprecated
	 */
	void displayBoard();
	
	/**
	 * Getter du nombre de lignes
	 * @return nombre de lignes du plateau
	 */
	int getRows();
	
	/**
	 * Getter du nombre de colonnes
	 * @return nombre de colonnes du plateau
	 */
	int getColumns();
	
	/**
	 * Getter de l'attribut squares de la classe
	 * @return array en deux dimensions avec toutes les cases du jeu
	 */
	Square [][] getSquares();
	
	/**
	 * Getter d'une case spécifique mémorisé dans l'attribut squares
	 * @param i Ligne de la case
	 * @param j Colonne de la case
	 * @return objet Square représentant la case souhaitée
	 */
	Square getSquare(int i, int j);
	
	/**
	 * Méthode responsable pour trouver une case voisine à partir d'une case
	 * de départ, appellée "sq", et une direction donnée
	 * 
	 * @param sq Case de départ
	 * @param dir Direction où la case voisine est localisée
	 * @return case voisine
	 */
	Square getNeighbor(Square sq, Direction dir);
	
	/**
	 * Fonction qui supprime tout autre tuyau de même couleur
	 * que le plot renseigné
	 * @param newTerminal
	 */
	void collapseSameColorPipe(Terminal newTerminal);
	
}
