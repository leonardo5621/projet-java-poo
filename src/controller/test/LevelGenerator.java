package controller.test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


import controller.Controller;
import model.Board;


class LevelGenerator {

	private Controller controller;

	@BeforeEach
	void setUp() throws Exception {
		Board board = new Board(5,5); 
		this.controller = new Controller(board);
		controller.processClick(0, 0);
		System.out.println("Controller Created");
	}

	@Test
	void test() {
		this.controller.showBoard();
	}

}
