package controller;

import model.Board;
import model.Direction;
import model.Pipe;
import model.Square;
import model.Terminal;

/**
 * Controleur général du jeu
 * Cette classe gère commandes click et touche clavier
 * @author leonardo
 *
 */
public class Controller {

	private IBoard boardGame;
	private Terminal currentPos;
	
	/**
	 *	Constructeur de Controller, il crée une nouvelle
	 *	Instance de la classe Board pour commencer le jeu
	 * 
	 * @param rows Nombre de lignes du plateau
	 * @param columns Nombre de colonnes du plateau
	 *
	 */
	public Controller(IBoard board) {
		this.boardGame = board;
	}
	
	/**
	 * Affichage du plateau en ligne de commance 
	 */
	public void showBoard() {
		boardGame.displayBoard();
	}
	
	/**
	 *  Getter de currentPos (court pour : currentPosition)
	 * @return le dernier plot cliqué par l'utilisateur
	 */
	public Terminal getPosition() {
		return this.currentPos;
	}
	
	/**
	 * 
	 * @return une array de deux dimensions avec toutes les cases du plateau
	 */
	public Square [][] getSquares(){
		return boardGame.getSquares();
	}
	
	/**
	 * Méthode retournant le tuyau associé au plot
	 * mémorisé par la classe controller
	 * @return Un objet de la classe Pipe représentant le tuyau du plot mémorisé
	 */
	public Pipe getBranchingPipe() {
		if(currentPos != null) {
			return currentPos.getPipe();
		} else {
			return null;
		}
	}
	
	/**
	 * Méthode appellé lorsque l'utilisateur clique sur une case
	 * 
	 * <p>
	 * Si la case contient un plot, ce plot sera mémorisé
	 * par la classe controller en tant qu'un attribut. En plus,
	 * si le nouveau plot est de la même couleur que le précédent,
	 * le tuyau qui l'utilisateur construisait sera supprimé.
	 * Par contre, si la case choisi n'a pas de plot, rien se passera 
	 * 
	 * @param newI Coordonné i de la case cliqué par l'utilisateur
	 * @param newJ Coordonné j de la case cliqué par l'utilisateur
	 */
	public void processClick(int newI, int newJ) {
		Square newSq = boardGame.getSquare(newI, newJ);
		
		if(currentPos != null && newSq.getTerminal() != null) {
			
			boardGame.collapseSameColorPipe(newSq.getTerminal());
			currentPos = newSq.getTerminal();
			
		} else if(currentPos == null && newSq.getTerminal() != null){
			currentPos = newSq.getTerminal();
		}
		
	}
	
	/**
	 * 	Cette méthode est responsable pour recevoir
	 * 	La commande donné par l'utilisateur appuyant sur les touches du clavier.
	 * 
	 * <p>
	 * Cette méthode communique au plot de départ la direction d'extension
	 * du tuyau
	 * 
	 * @param dir Direction de progression du tuyau
	 */
	public void processKey(Direction dir) {
		if( currentPos != null) {
			System.out.println("DIRECTIOn RECU "+dir);
			currentPos.extend(dir);
			
		}
	}

}
