package model;

import java.awt.Color;
import java.util.HashMap;
import java.util.Map;

/**
 * Classe représentant les tuyaux du jeu
 * @author leonardo
 *
 */
public class Pipe {
	
	private static final int ORIGININDEX = 0;
	private ColorSet color;
	private HashMap<Integer, Square> path;
	private boolean complete;

	
	public Pipe(ColorSet color, Square origin) {
		this.color = color;
		this.path = new HashMap<Integer, Square>();
		this.path.put(ORIGININDEX, origin);
		this.complete = false;
		origin.addToPipe(this);
	}
	
	/**
	 * Getter du status du tuyau
	 * Ce status est représenté par l'attribut "complete", si le tuyau relie
	 * les deux plots de même couleur sur le plateau, sa valeur sera true. Sinon, il prend
	 * la valeur false
	 * @return attribut boolean complete
	 */
	public boolean checkComplete() {
		return this.complete;
	}
	
	/**
	 * Getter du HashMap gardant les cases faisant partie du tuyau.
	 * Les valeurs clé son des indices entier de zero à n, où n c'est la taille du tuyau
	 * @return HashMap avec toutes les cases mémorisées
	 */
	public HashMap<Integer, Square> getPath(){
		return path;
	}
	
	/**
	 * Getter de la couleur du tuyau
	 * @return Couleur listé dans l'énumération ColorSet
	 */
	public ColorSet getColor() {
		return color;
	}
	
	/**
	 * Commande pour que le tuyau revienne en arrière
	 */
	public void goBack() {
		int pathLen = path.size();
		if(pathLen > 1) {
			Square toRemove = path.remove(pathLen-1);
			toRemove.removeFromPipe();
			if(this.checkComplete()) {
				this.complete = false;
			}
		}
		
	}
	
	/**
	 * Commande pour supprimer ce tuyau
	 */
	public void clear() {
		int len = path.size();
		for(int i=1; i<len; i++) {
			Square sq = path.remove(i);
			System.out.println("Removing "+sq);
			sq.removeFromPipe();		
		}
		
	}
	
	/**
	 * Méthode validant l'extension du tuyau demandée
	 * @param dir Direction de l'extension
	 * @return true si l'opération a réussi, false sinon
	 */
	public boolean extendPipe(Direction dir) {
		int pathLen = path.size();
		Square square = path.get(pathLen-1).getNeighboringSquare(dir);
		if(path.containsValue(square)) {
			this.goBack();
			return true;
		}
		
		//Si le tuyau est arrive à relier les deux plots
		//La progression s'arrête
		if(this.checkComplete()) return false;
		
		//Vérifier si la Case a un plot
		if(square.getTerminal() != null) {
			// Ensuite, vérifier si c'est de la même couleur qu'à l'origine
			
			 if(square.getSquareColor() == path.get(0).getSquareColor()) {
				System.out.println("Path Completed");
				path.put(path.size(), square);
				square.addToPipe(this);
				this.complete = true;
				return true;
			} else {
				return false;
			}
			
		} else if(square.getPipe() != null) {
			return false;
		} else {
			path.put(path.size(), square);
			square.addToPipe(this);
			return true;
		}
		
	}
	
	

}
