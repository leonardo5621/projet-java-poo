package model;

import java.awt.Color;

/**
 * Classe représentant les plots du plateau
 * Son rôle principal c'est de donner la commande pour
 * faire progresser ou supprimer le tuyau associé au plot 
 * @author leonardo
 *
 */
public class Terminal {
	
	private ColorSet color;
	private Pipe pipe;
	
	/**
	 * Constructeur initialisant l'objet en gardant la case d'origine et
	 * la couleur du plot en tant qu'attributs
	 * @param color Couleur du plot donnée
	 * @param origin Case associée au plot
	 */
	public Terminal(ColorSet color, Square origin) {
		this.color = color;
		this.pipe = new Pipe(color, origin);
	}
	
	/**
	 * Cette méthode est responsable pour donner la commande
	 * à l'instance de la classe tuyau, gardé en paramètres dans la classe,
	 * pour le faire progresser à une direction donnée
	 * @param nextStep Direction pour faire progresser le tuyau
	 */
	public void extend(Direction nextStep) {
		this.pipe.extendPipe(nextStep);
		
	}
	
	/**
	 * Méthode passant la commande pour l'objet de la classe tuyau (appellé Pipe dans ce cas )
	 * pour le supprimer
	 */
	
	public void collapse() {
		this.pipe.clear();
	}
	
	/**
	 * Getter du tuyau associé au plot
	 * @return objet de la classe Pipe 
	 */
	public Pipe getPipe() {
		return this.pipe;
	}
	
	/**
	 * Getter de la couleur du plot
	 * @return Couleur appartenant à l'énumération ColorSet 
	 */
	public ColorSet getColor() {
		return this.color;
	}

}
