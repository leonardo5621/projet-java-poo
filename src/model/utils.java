package model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Random;
import java.util.Set;

public class utils {

	public utils() {
		// TODO Auto-generated constructor stub
	}
	
	public static boolean checkPoint(Point p1, int i, int j) {
		if(p1.getRow() == i && p1.getCol() == j) {
			return true;
		} else {
			return false;
		}
	}
	
//	public static Map<ColorSet, Point[]> getDefaultLevel(){
//		return DefaultLevels.defaultSize5;
//	}
	
	public static HashMap<String, Point[]> generateLevel(int size){
		
		Random rand = new Random();
		
		
		HashMap<String, Point []> plotSetup = new HashMap<String, Point[]>();
		plotSetup.put("upperBoard", new Point[size]);
		plotSetup.put("bottomBoard", new Point[size]);
		
		if(size == 5) {
			Map<ColorSet, Point[]> defaultSet = DefaultLevels.defaultSize5;
			 ArrayList<Point[]> valueList = new ArrayList<Point[]>(defaultSet.values());
			
			for(int i=0; i<size;i++) {
				plotSetup.get("upperBoard")[i] = valueList.get(i)[0];
				plotSetup.get("bottomBoard")[i] = valueList.get(i)[1]; 
			}
			return plotSetup;
		}
		
		for(int i=0; i<size;i++) {
			plotSetup.get("upperBoard")[i] = new Point(0, i);
			plotSetup.get("bottomBoard")[i] = new Point(size-1, i); 
		}
		
		int[] arr = new int[size];
	    for (int i = 0; i < arr.length; i++) {
	    	arr[i] = 1+rand.nextInt(size-2);
	    }
	         
		Set<Integer> set = new HashSet<Integer>();

		for(int j = 0; j < size; j++){
		  set.add(arr[j]);
		}
		
		
		Integer [] randArray = set.toArray(new Integer[set.size()]);
		
		for(int i : randArray) {

			double r = rand.nextDouble();
			int neighborIndex;
			if(r > 0.5) {
				neighborIndex = -1;
			} else {
				neighborIndex = 1;
			}
			
			Point randPoint = plotSetup.get("upperBoard")[i];
			System.out.println(i+neighborIndex);
			Point neighboorPoint = plotSetup.get("upperBoard")[i+neighborIndex];
			randPoint.setRow(randPoint.getRow()+1);
			neighboorPoint.setCol(randPoint.getCol());
			neighboorPoint.setRow(randPoint.getRow()-1);
			randPoint = plotSetup.get("bottomBoard")[i];
			neighboorPoint = plotSetup.get("bottomBoard")[i-1];
			randPoint.setRow(randPoint.getRow()-1);
			neighboorPoint.setCol(randPoint.getCol());
			neighboorPoint.setRow(randPoint.getRow()+1);	
			
		}
		
		return plotSetup;
		
	}
	

}
