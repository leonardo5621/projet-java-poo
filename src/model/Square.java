package model;

import java.awt.Color;

import controller.IBoard;

/**
 * Classe représentant les cases du plateau.
 * L'objet mémorise le plot ou tuyau associé à la case, ainsi qu'une
 * référence du plateau. 
 * @author leonardo
 *
 */
public class Square {
	
	private Terminal terminal;
	private IBoard board;
	private Pipe passingPipe;
	
	/**
	 * Constructeur de la case, il prend en entrée l'objet du plateau et le garde en attribut
	 * @param board instance du plateau associé à la case
	 */
	public Square(IBoard board) {
		this.board = board;
	}
	
	/**
	 * Méthode responsable pour attribuer un nouveau plot (objet de la classe Terminal)
	 * à la case
	 * @param terminal
	 */
	public void assignTerminal(Terminal terminal) {
		this.terminal = terminal;
	}
	
	/**
	 * Méthode ajoutant établissant un lien entre la case et un tuyau 
	 * @param pipe tuyau associé à la case
	 */
	public void addToPipe(Pipe pipe) {
		this.passingPipe = pipe;
	}
	
	/**
	 * Getter du tuyau associé à la case
	 * @return objet représentant le tuyau passant sur la case
	 */
	public Pipe getPipe() {
		return passingPipe;
	}
	
	/**
	 *Méthode responsable pour supprimer le lien entre la case et le tuyau 
	 */
	public void removeFromPipe() {
		this.passingPipe = null;
	}
	
	public Square getNeighboringSquare(Direction dir) {
		return this.board.getNeighbor(this, dir);
	}
	
	
	/**
	 * Getter du plot associé à la case
	 * @return objet de la classe Terminal représentant le plot de cette case
	 */
	public Terminal getTerminal() {
		return this.terminal;
	}
	
//	public void collapsePipe() {
//		if(passingPipe != null) {
//			passingPipe.clear();
//		}
//	}
	
	/**
	 * Si la case a un plot ou tuyau associé, cette méthode retourne sa couleur
	 * @return couleur de la case
	 */
	public ColorSet getSquareColor() {
		if(terminal != null) {
			return terminal.getColor();
		} else if(passingPipe != null) {
			return passingPipe.getColor();
		} else {
			return null;
		}
		
	}

}
