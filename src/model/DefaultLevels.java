package model;

import java.util.HashMap;
import java.util.Map;

public class DefaultLevels {
	
	public static Map<ColorSet, Point[]> defaultSize5;
	public static Map<ColorSet, Point[]> defaultSize5_2;
	static {
		defaultSize5 = new HashMap<>();
		defaultSize5_2 = new HashMap<>();
		Point[][] array5 = new Point[][] {{new Point(0,0), new Point(4,1)},
											{new Point(0,2), new Point(3,1)},
											{new Point(1,2), new Point(4,2)},
											{new Point(1,4), new Point(4,4)},
											{new Point(0,4), new Point(4,3)}};
		
		Point[][] array5_2 = new Point[][] {{new Point(0,0), new Point(2,4)},
												{new Point(1,0), new Point(2,3)},
												{new Point(3,0), new Point(3,3)},
												{new Point(1,1), new Point(1,3)},
												{new Point(4,0), new Point(3,4)}};
												
		defaultSize5.put(ColorSet.DARKBLUE, array5[0]);
		defaultSize5.put(ColorSet.RED, array5[1]);
		defaultSize5.put(ColorSet.SALMON, array5[2]);
		defaultSize5.put(ColorSet.STEELBLUE, array5[3]);
		defaultSize5.put(ColorSet.BLACK, array5[4]);
		
		defaultSize5_2.put(ColorSet.DARKBLUE, array5_2[0]);
		defaultSize5_2.put(ColorSet.RED, array5_2[1]);
		defaultSize5_2.put(ColorSet.SALMON, array5_2[2]);
		defaultSize5_2.put(ColorSet.STEELBLUE, array5_2[3]);
		defaultSize5_2.put(ColorSet.BLACK, array5_2[4]);
	}
	
	
	public DefaultLevels() {
		// TODO Auto-generated constructor stub
	}

}
