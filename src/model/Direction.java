package model;

/**
 * Énumération des directions possibles
 * de progression des tuyaux 
 * @author leonardo
 *
 */
public enum Direction {
	RIGHT,
	LEFT,
	UP,
	DOWN
}
