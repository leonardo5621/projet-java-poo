package model;

import java.awt.Color;

/**
 * Ensemble de couleurs pour le jeu
 * @author leonardo
 *
 */
public enum ColorSet {
	RED(255, 69, 0, "R"),
	SALMON(255, 165, 0, "S"),
	DARKBLUE(0, 0, 139, "DB"),
	STEELBLUE(70, 130, 180, "SB"),
	BLACK(0, 0, 0, "B"),
	GREY(128, 128, 128, "G"),
	WHITE(255, 255, 255, "W");
	
	private Color color;
	public Color getColor() {
		return color;
	}

	public void setColor(Color color) {
		this.color = color;
	}

	public String getColorChar() {
		return colorChar;
	}

	public void setColorChar(String colorChar) {
		this.colorChar = colorChar;
	}

	public String colorChar;
	
	/**
	 * Création d'un couleur à partir des valeurs RGB renseignées
	 * @param r
	 * @param g
	 * @param b
	 * @param colorChar Caractère unique représentant la couleur
	 */
	private ColorSet(int r, int g, int b, String colorChar) {
		this.color = new Color(r, g, b);
		this.colorChar = colorChar;
	}
	
	
}
