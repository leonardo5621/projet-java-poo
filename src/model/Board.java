package model;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import controller.IBoard;

/**
 * Plateau du jeu, son rôle principal c'est de mémoriser les cases
 * et ses positions respectives sur le plateau
 * @author leonardo
 *
 */
public class Board implements IBoard{

	private Square [][] squares;
	private int columns;
	private int rows;
	private Point[] upperTerminals;
	private Point[] bottomTerminals;
	
	
	/**
	 * Le constructeur lance le jeu en créant des instances
	 * de Square pour chaque position du plateau
	 * 
	 * @param rows nombre de lignes du plateau
	 * @param columns nombre de colonnes du plateau
	 */
	public Board(int rows, int columns) {
		
		this.rows = rows;
		this.columns = columns;
		this.squares = new Square[rows][columns];
		this.startBoard();
	}
	
	@Override
	public void startBoard() {
		
		for(int i=0; i<rows; i++) {
			for(int j=0; j<columns; j++) {
				squares[i][j] = new Square(this);
			}
		}
		
		HashMap<String, Point[]> plotSetUp = utils.generateLevel(rows);
		this.upperTerminals = plotSetUp.get("upperBoard");
		this.bottomTerminals = plotSetUp.get("bottomBoard");
		for(int k=0; k<rows;k++) {
			Point p1 = upperTerminals[k];
			Point p2 = bottomTerminals[k];
			ColorSet chosenColor = ColorSet.values()[k];
			Terminal toAssignUpper = new Terminal(chosenColor, squares[p1.getRow()][p1.getCol()]);
			squares[p1.getRow()][p1.getCol()].assignTerminal(toAssignUpper);
			
			Terminal toAssignBottom = new Terminal(chosenColor, squares[p2.getRow()][p2.getCol()]);
			squares[p2.getRow()][p2.getCol()].assignTerminal(toAssignBottom);
		}
	}
	
	public void reset() {
		this.collapseAll(upperTerminals);
		this.collapseAll(bottomTerminals);
	}
	
	public void collapseAll(Point[] pointArray) {
		for(Point p1 : pointArray) {
			Terminal t =  squares[p1.getRow()][p1.getCol()].getTerminal();
			t.collapse();
		}
	}
	
	public void findTerminalSameColor(Point[] pointArray,Terminal newTerminal) {
		for(Point p1 : pointArray) {
			Terminal t =  squares[p1.getRow()][p1.getCol()].getTerminal();
			if(t.getColor() == newTerminal.getColor() && t != newTerminal) {
				t.collapse();
			}
			
		}
	}
	
	public void collapseSameColorPipe(Terminal newTerminal) {
		this.findTerminalSameColor(upperTerminals, newTerminal);
		this.findTerminalSameColor(bottomTerminals, newTerminal);
	}
	
	
	@Override
	public Square [][] getSquares(){
		return this.squares;
	}
	
	@Override
	public Square getSquare(int i, int j) {
		return this.squares[i][j];
		
	}
	
	@Override
	public Square getNeighbor(Square sq, Direction dir) {
		
		for(int i=0; i<rows; i++) {
			for(int j=0; j<columns; j++) {
				if(squares[i][j] == sq) {
					return this.getNeighboringSquare(i, j, dir);
				}
			}
		}
		return null;
		
	}
	
	/**
	 * Cette méthode exécute la logique pour trouver une case voisine
	 * à partir des coordonnées de la case de départ.
	 * 
	 * Si la direction donne une case hors du plateau, cette méthode retourne
	 * la case de départ
	 * 
	 * @param i Coordonné ligne de la case de départ
	 * @param j Coordonné colonne de la case de départ
	 * @param dir Direction où la case voisine est localisée
	 * @return Case voisine
	 */
	public Square getNeighboringSquare(int i, int j, Direction dir) {
		
		switch(dir) {
		case RIGHT:
			j = j == columns-1 ? j : j+1;
			break;
		case LEFT:
			j = j == 0 ? 0 : j-1;
			break;
		case UP:
			i = i == 0 ? 0 : i-1;
			break;
		case DOWN:
			i = i == rows-1 ? i : i+1;
			break;
		}
		
		return squares[i][j];
	}
	
	@Override
	public int getRows() {
		return rows;
	}
	
	@Override
	public int getColumns() {
		return columns;
	}
	
	@Override
	public void displayBoard() {
		for(int i=0; i<rows; i++) {
			System.out.print(" | ");
			for(int j=0; j<columns; j++) {
				if(squares[i][j].getTerminal() != null) {
					System.out.print(" T ");
				} else {
					Pipe passPipe = squares[i][j].getPipe(); 
					if( passPipe != null) {
						System.out.print(passPipe.getColor().colorChar);
					} else {
						System.out.print(" X ");
					}
				}
			}
			System.out.print(" | ");
			System.out.println("  ");
		}
		System.out.println("  ");
	}
	

}
