package model;

public class Point {
	
	private int i;
	private int j;
	
	public Point(int i, int j) {
		this.i = i;
		this.j = j;
	}
	
	public int getRow() {
		return i;
	}
	
	public int getCol() {
		return j;
	}
	
	public void setRow(int i) {
		this.i = i;
	}
	
	public void setCol(int j) {
		this.j = j;
	}
	
//	@Override
//	public boolean equals(Object obj) {
//		if(obj instanceof Point) {
//			Point castObj = (Point) obj;
//			if( castObj.getX() == this.getX() && castObj.getY()==this.getY()) {
//				return true;
//			} else {
//				return false;
//			}
//		} else {
//			return false;
//		}
//		
//	}
}
