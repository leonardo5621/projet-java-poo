package view;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.*;

import controller.Controller;
import model.Board;
import model.ColorSet;
import model.Direction;
import model.Square;
import model.Terminal;

public class Main {


	/***
	 * Initialisation de variables
	 */
   private JFrame mainFrame;
   private JPanel controlPanel;
   int gridSize = 5 ; 
   int caseSize = 100 ; 
   int width = gridSize * caseSize ; 
   int frameWidth = width+50; 
   Grid gridGenerator ; 
   JTextField textField;
   Board tableDeJeu ;
   Controller controller ;
   Square [][] plateau ; 
   Square [][] plateauNonModifie ; 
   Square selectedSquare ;
   Box selectedBox ; 
   ColorSet selectedColor;
   
   
   /**
    * Constructeur par default
    */
   public Main(){
	   this.tableDeJeu = new Board(gridSize,gridSize) ; 
	   selectedColor = ColorSet.GREY ;
	   this.plateau = this.tableDeJeu.getSquares();
	   this.plateauNonModifie = this.tableDeJeu.getSquares();
	   this.controller = new Controller (tableDeJeu) ; 
	   prepareGUI();
   }
   
   
   /*
    * Actualiser l'affichage du jeu après une action
    * 
    */
   private void removeAll () {
	   //mainFrame.getContentPane().removeAll();
	   controlPanel.removeAll();
	   showGridLayout() ; 
   }
   
   /**
    * Fonction permettant de revenir à l'écran d'accueil du jeu
    */
   private void goBack () {
	   //mainFrame.getContentPane().removeAll();

	   
	   controlPanel.removeAll();
	   showStartView() ; 
   }
   
   /*
    * Fonction permettant de remettre la taille initiale de la grille lors de l'affichage de la page d'accueil
    * Si l'utilisateur choisi une configuration de grille plus grande que la taille initiale de la fenetre de jeu, il y aura une adaptation automatique
    */
   void reinistialize() {
	   width = 500; 
	   frameWidth = width+50; 
   }
   
   
   /*
    * Fonction permetant d'afficher l'écran d'accueil du jeu
    * Elle est appélée par la fonction permettant d'y revenir
    */
   private void showStartView () {
	   	reinistialize() ; 
   		JPanel panel = new JPanel();
	      panel.setBackground(Color.black);
	      panel.setPreferredSize(new Dimension(width,width));
	      GridLayout layout = new GridLayout(3,1);
	      panel.setLayout(layout);  
	      
	      JLabel jlabel = new JLabel("Bienvenue sur Flow Free",SwingConstants.CENTER);
	      jlabel.setFont(new Font("Verdana",1,20));
	      jlabel.setForeground(Color.white);
	      panel.add(jlabel);
	      
	      textField = new JTextField(8);
	      //textField.setBounds(5, 5, 280, 50); // to get height, set large font
	      textField.setFont(textField.getFont().deriveFont(50f));
	      panel.add(textField);
	        
	      
	      JButton myButton = new JButton("Commencer le jeu");
	      myButton.addActionListener(new ActionListener() {
	            @Override
	            public void actionPerformed(ActionEvent e) {
	                //JOptionPane.showMessageDialog(myButton, this, "COOL", caseSize);
	            	String text = textField.getText() ; 
	            	if (text == null || text.equals("")) {
	            		return ; 
	            	}
	            	int value  = Integer.parseInt(textField.getText()) ;
	            	if(value<=0) {
	            		return ;
	            	}
	            	gridSize = value  ; 
	         	   tableDeJeu = new Board(gridSize,gridSize) ; 
	        	   plateau = tableDeJeu.getSquares();
	        	   plateauNonModifie = tableDeJeu.getSquares();
	        	   controller = new Controller (tableDeJeu) ; 
	        	   
	        	   
	            	removeAll() ; 
	                System.out.println("yes "+value) ; 
	            }
	      });
	      
	      panel.add(myButton);
	        
	      controlPanel.add(panel);
	      mainFrame.resize(frameWidth, frameWidth);
	      mainFrame.setVisible(true);  
	      
   }
   
   /**
    * Mise en place de la fenetre  principale du jeu
    */
   private void prepareGUI(){
      mainFrame = new JFrame("Flow free");
      mainFrame.setSize(frameWidth,frameWidth);
      mainFrame.addWindowListener((WindowListener) new WindowAdapter() {
         public void windowClosing(WindowEvent windowEvent){
            System.exit(0);
         }        
      });    
      controlPanel = new JPanel();
      controlPanel.setLayout(new FlowLayout());
      
      JScrollPane pane = new JScrollPane(controlPanel);
      mainFrame.add(pane);
      mainFrame.setVisible(true);  
   }
   
   /*
    * Recalcul de la nouvelle taille de la fenetre apres choix de la taille de la grille de jeu par l'utilisateur
    */
   public void updateValues () {
	   

	   
	   width = gridSize * caseSize ; 
	   frameWidth = width+50; 
	   this.gridGenerator = new Grid(gridSize) ; 
   }
   
   /**
    * Lancer la mise à jour de la grille
    */
   
  void updateGrid(){
	  plateau = controller.getSquares() ;
	  removeAll() ; 
  }
  

   /*
    * 
    * Afficher la grille de jeu
    */
   private void showGridLayout(){
	   
	  updateValues() ;
      JPanel panel = new JPanel();
      //panel.setFocusable(true);
      //panel.requestFocusInWindow();
      controlPanel.requestFocus() ; 
      KeyListener keyListener =  new KeyListener() {
		  
		  public void keyPressed(KeyEvent e) {
		  }

			@Override
			public void keyTyped(KeyEvent e) {
			}

			@Override
			public void keyReleased(KeyEvent e) {
				if(selectedBox == null) return ;
				int line = selectedBox.line ; 
				int column = selectedBox.line ; 
				if(selectedSquare == null) {
					return ;
				}
				int keyCode = e.getKeyCode();
				 
			    switch( keyCode ) { 
			        case KeyEvent.VK_UP:
				    	System.out.println("UP") ; 
			            controller.processKey(Direction.UP) ; 
		            	controlPanel.removeKeyListener(this);
			            updateGrid() ;
			            break;
			        case KeyEvent.VK_DOWN:
				    	System.out.println("DOWN") ; 
			            controller.processKey(Direction.DOWN) ; 
		            	controlPanel.removeKeyListener(this);
			            updateGrid() ;
		
			            break;
			        case KeyEvent.VK_LEFT:
				    	System.out.println("LEFT") ; 
			            controller.processKey(Direction.LEFT) ; 
		            	controlPanel.removeKeyListener(this);
			            updateGrid() ;
			            break;
			        case KeyEvent.VK_RIGHT :
				    	System.out.println("RIGTH") ; 
			            controller.processKey(Direction.RIGHT) ; 
		            	controlPanel.removeKeyListener(this);
			            updateGrid() ; 
			            break;
			     }
			    
			} 
	  
      } ; 
      controlPanel.addKeyListener(keyListener);
      
      panel.setBackground(Color.white);
      panel.setPreferredSize(new Dimension(width,width));
      GridLayout layout = new GridLayout(this.gridSize+1,this.gridSize+1,5,5);
      panel.setLayout(layout);  
      int [][] grid = gridGenerator.getGrid() ;
      for(int i = 0 ; i < gridSize; i++) {
    	  for (int j = 0; j < gridSize; j++) {
        	  //Box currentBox = new Box(caseSize,getColor(grid[i][j])) ;
    		  Box currentBox  ;
    		  if(this.plateau[i][j].getTerminal() != null || this.plateau[i][j].getPipe()!=null) {
    			  if(this.plateau[i][j].getPipe()!=null) {
    				  currentBox = new Box(caseSize,this.plateau[i][j].getPipe().getColor(),i,j,false) ;
    			  }
    			  else {
    				  currentBox = new Box(caseSize,this.plateau[i][j].getTerminal().getColor(),i,j,true) ;
    			  }
    		  }
    		  else {
    			  currentBox = new Box(caseSize,ColorSet.GREY,i,j,false) ;
    		  }
    		  
    		  int line = currentBox.line ; 
    		  int column = currentBox.column ;
				
    		 
    		  
    		  currentBox.addMouseListener(new MouseAdapter() {
    			  @Override
		            public void mouseClicked(MouseEvent event) {
		               selectedSquare = plateau[line][column] ; 
		               controller.processClick(line,column) ; 
		               selectedBox = currentBox ; 
		            }
    			  
		            @Override
		            public void mouseEntered(MouseEvent event) {
		                
		            }

		            @Override
		            public void mouseExited(MouseEvent event) {
		              
		            }    
		        });
    		  panel.add(currentBox) ; 
    		 
    	  }
      }
      
      JButton accueil = new JButton("Accueil");
      accueil.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
            	tableDeJeu.reset();
            	controlPanel.removeKeyListener(controlPanel.getKeyListeners()[0]) ; 
            	goBack() ; 
            }
      });
      
      JButton reset = new JButton("Reprendre");
      reset.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                plateau = plateauNonModifie ; 
                tableDeJeu.reset();
            	controlPanel.removeKeyListener(controlPanel.getKeyListeners()[0]) ; 
            	removeAll() ; 
            }
      });
      
      panel.add(accueil);
      panel.add(reset);
      
      controlPanel.add(panel);
      mainFrame.resize(frameWidth, frameWidth);
      mainFrame.setVisible(true);  
   }
   

   /**
    * Méthode main : lancement du jeu
    * @param args
    */
   public static void main(String[] args){
	   Main swingLayoutDemo = new Main();  
	   //swingLayoutDemo.showGridLayout();    
	   swingLayoutDemo.showStartView(); 
   }
   
}
