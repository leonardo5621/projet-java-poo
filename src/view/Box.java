package view;


import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Shape;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Rectangle2D;
import java.awt.event.*;
import javax.swing.JPanel;

import model.ColorSet;
import model.Square;

public class Box extends JPanel  {

	private int size ; 
	public  ColorSet colorSet ; 
	public  boolean wasGrey ; 
	public int line ; 
	public int column ; 
	boolean terminal ; 
	
    public Box (int size,ColorSet color,int line, int column,boolean terminal)
    {
    	this.size = size ; 
    	this.colorSet = color ;
    	this.wasGrey = (color == ColorSet.GREY) ; 
    	this.line = line ;
    	this.column = column ;
    	this.terminal = terminal ; 
    	
    }
    	
	protected void paintComponent(Graphics g) {
		Graphics2D g2 = (Graphics2D) g;
	    super.paintComponent(g2);
	    g2.setColor(this.colorSet.getColor());
	    g2.fill(new Rectangle2D.Double(0,0,this.size, this.size)) ; 

	    

    }
	
	
    
}
