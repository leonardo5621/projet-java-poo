package view;

public class Grid {
	
	int size ; 
	
	public  Grid(int size) {
		this.size = size ; 
		
	}
	
	public int [] [] getGrid () {
		int [][] grid = new int [size][size] ; 
		int count = 0 ; 
		for(int i = 0; i < size; i++) {
			for(int j = 0; j < size; j++) {
				int value = ((int)(Math.random()*10000))%10 ; 
				value = Math.random() >0.5 ? value : 0 ; 
				grid[i][j] =  count < 7 ? value : 0 ; 
				if(value>0) count ++ ; 
			}
		}
		return grid ; 
	}
	
}
