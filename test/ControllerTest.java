package backend.test;

import static org.junit.jupiter.api.Assertions.*;

import java.util.HashMap;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import backend.BoardTest;
import backend.ColorSet;
import backend.Controller;
import backend.Direction;
import backend.Pipe;
import backend.Square;
import backend.Terminal;

class ControllerTest {
	
	private Controller controller;
	@BeforeEach
	void setUp() throws Exception {
		BoardTest board = new BoardTest(5,5); 
		this.controller = new Controller(board);
		controller.processClick(0, 0);
		System.out.println("Controller Created");
	}
	
	@Test
	void standStill() {
		/*
		 * Nada acontece feijoada 
		 **/
		Terminal firstPos = controller.getPosition();
		controller.processKey(Direction.LEFT);
		controller.processKey(Direction.UP);
		Terminal secPos = controller.getPosition();
		assertEquals(firstPos, secPos, "The position should not have changed");
	}
	
	@Test
	void completePath() {
		
		controller.processKey(Direction.DOWN);
		Terminal thirdPos = controller.getPosition();
		controller.processKey(Direction.DOWN);		
		Pipe pipe = controller.getBranchingPipe();
		
		assertEquals(3, pipe.getPath().size(), "There should be 3 squares at the path");
		
		controller.processKey(Direction.DOWN);
		controller.processKey(Direction.DOWN);
		controller.showBoard();
		
		Pipe pipe2 = controller.getBranchingPipe();
		boolean pathComplete = pipe2.checkComplete();
		assertTrue(pathComplete, "Pipe should have been completed");
	}
	
	@Test
	void notValidExtension() {
		controller.processKey(Direction.DOWN);
		controller.processKey(Direction.RIGHT);
		controller.processKey(Direction.UP);
		
		Pipe pipe2 = controller.getBranchingPipe();	
		assertEquals(3, pipe2.getPath().size(), "There should be 3 squares at the path");
	}
	
	@Test
	void pathIntersection() {
		controller.processKey(Direction.DOWN);
		controller.processKey(Direction.DOWN);
		Terminal firstTerminal = controller.getPosition();
		
		controller.processClick(0, 1);
		controller.processKey(Direction.DOWN);
		controller.processKey(Direction.LEFT);
		Terminal secondTerminal = controller.getPosition();
		
		Pipe pipe = controller.getBranchingPipe();
		
		assertTrue( firstTerminal != secondTerminal, "Different terminals");
		assertEquals(2, pipe.getPath().size(), "There should be no path intersection");
		
	}
	
	@Test
	void collapsePipeByClick() {
		/**
		 * Acho que vou precisar revisar esse método aqui
		 */
		Terminal firstSq = controller.getPosition();
		Pipe firstBranch = firstSq.getPipe();
		controller.processKey(Direction.DOWN);
		controller.processKey(Direction.DOWN);
		assertEquals(3, firstBranch.getPath().size(), "The pipe should be collapsed");
		controller.processClick(4, 0);
		assertEquals(1, firstBranch.getPath().size(), "The pipe should be collapsed");
		controller.processKey(Direction.UP);		
		assertTrue(firstSq.getPipe() != null, "The pip should still exist");
	}
	
	@Test
	void goBackTest() {
		/*
		 * Trocar o HashMap pour outra coisa talvez?
		 */
		Pipe pipe = controller.getBranchingPipe();
		HashMap<Integer, Square> squareMap = pipe.getPath(); 
		controller.processKey(Direction.DOWN);
		controller.processKey(Direction.DOWN);
		Square lastSquare = squareMap.get(2);
		assertSame(pipe, lastSquare.getPipe());
		controller.processKey(Direction.UP);
		assertNull(lastSquare.getPipe());
		
	}
	
}
